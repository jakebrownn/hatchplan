# Hatchplan Code Challenge
To the team at Hatch / VouchedFor,

This code challenge was very interesting.

I attempted to keep everything as dynamic as possible. For example, instead of using a fixed input field structure, I looped over the data that was provided in the instance that more groups of data would be added in the future. This approach left me scratching my head on a few occasions, but ultimately I'm quite pleased with the outcome.

There are a couple of bugs I would have liked to have fixed with more time. These are:

* Changing a monetary input field does not update the slider max-value.
* Inputting a greater value in the monetary input field results in the green 'total savings' text saying you've saved money. 

Both of these bugs do fix themselves on page refresh.

In hindsight, I probably should have gone with the fixed-input-structure approach, or at least asked about it, as it would have simplified the app. In short, I think I've gone overkill?

You may also notice whilst looking through the repository, a lot of the codebase can be simplified further e.g. removing index files in ./actions and ./reducers, slimming-down the SASS folder structure, utilising local state and completely removing the use of Redux etc. However, I felt it was important to give you a more well-rounded understanding of my ability and how these 'bloated bits' may have been required for scaling the application in the future.

I look forward to hearing your feedback.

Many thanks!

-Jake

Link to app: [https://epic-goldberg-5f5ca2.netlify.com/](https://epic-goldberg-5f5ca2.netlify.com/)
