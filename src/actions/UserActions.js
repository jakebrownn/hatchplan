import {
  FETCH_USER_DATA,
  USER_DATA_FETCHED,
  AMOUNT_FIELD_CHANGED,
  FROM_AGE_FIELD_CHANGED,
  SLIDER_CHANGED,
  FEEDBACK_SUBMITTED,
  FEEDBACK_SUCCESS,
  TO_AGE_FIELD_CHANGED
} from './types';
import DummyData from '../utils/data';

export const fetchData = () => (dispatch) => {
  dispatch({ type: FETCH_USER_DATA });

  // 'Fake' waiting a response from the server / API.
  // Usually would be a try/catch block.
  setTimeout(() => {
    const hatchData = localStorage.getItem("hatchData");

    // If no localStorage has been set
    if (hatchData === null) {
      const { incomes, expenditures } = DummyData;

      // Add DummyData to state
      dispatch({ type: USER_DATA_FETCHED, payload: { incomes, expenditures } });

      // Otherwise, add localStorage to state
    } else {
      dispatch({ type: USER_DATA_FETCHED, payload: JSON.parse(hatchData) });
    }
  }, 750);
};

// When user submits feedback
export const submitFeedback = (feedback) => (dispatch) => {
  dispatch({ type: FEEDBACK_SUBMITTED });

  // 'Fake' waiting a response from the server / API.
  // Usually would be a try/catch block.
  setTimeout(() => {
    console.log(`The user feedback was ${feedback}!`)

    dispatch({ type: FEEDBACK_SUCCESS });
  }, 750);
};

// When amount value is changed
export const amountChanged = (dataType, row, input) => {
  return {
    type: AMOUNT_FIELD_CHANGED,
    payload: { dataType, row, input }
  };
};

// When from_age value is changed
export const fromAgeChanged = (dataType, row, input) => {
  return {
    type: FROM_AGE_FIELD_CHANGED,
    payload: { dataType, row, input }
  };
};

// When to_age value is changed
export const toAgeChanged = (dataType, row, input) => {
  return {
    type: TO_AGE_FIELD_CHANGED, 
    payload: { dataType, row, input }
  };
};

// When slider value is changed
export const sliderChanged = (sliderName, value) => {
  return {
    type: SLIDER_CHANGED,
    payload: { sliderName, value }
  };
};
