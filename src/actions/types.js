export const FETCH_USER_DATA = 'fetch_user_data';
export const USER_DATA_FETCHED = 'user_data_fetched';
export const AMOUNT_FIELD_CHANGED = 'amount_field_changed';
export const FROM_AGE_FIELD_CHANGED = 'from_age_field_changed';
export const TO_AGE_FIELD_CHANGED = 'to_age_field_changed';
export const SLIDER_CHANGED = 'slider_changed';
export const FEEDBACK_SUBMITTED = 'feedback_submitted';
export const FEEDBACK_SUCCESS = 'feedback_success';
