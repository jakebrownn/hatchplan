import React from 'react';
import { Switch, Route } from 'react-router-dom';
import HomeScreen from './screens/HomeScreen';
import BlogScreen from './screens/BlogScreen';

const Router = () => {
  return (
    <Switch>
      <Route exact path="/" component={HomeScreen} />
      <Route exact path="/blog" component={BlogScreen} />
    </Switch>
  );
};

export default Router;
