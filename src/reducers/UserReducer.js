import {
  FETCH_USER_DATA,
  USER_DATA_FETCHED,
  AMOUNT_FIELD_CHANGED,
  FROM_AGE_FIELD_CHANGED,
  TO_AGE_FIELD_CHANGED,
  SLIDER_CHANGED,
  FEEDBACK_SUBMITTED,
  FEEDBACK_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
  dataFetching: false,
  incomes: [],
  expenditures: [],
  sliders: [],
  dataFetched: false,
  feedbackSubmitting: false,
  feedbackRecieved: false
};

export default (state = INITIAL_STATE, action) => {
  let dataType;
  let row;
  let input;

  switch (action.type) {
    case FETCH_USER_DATA:
      return { ...state, dataFetching: true };
    case USER_DATA_FETCHED:
      const { incomes, expenditures, sliders } = action.payload;
      return {
        ...state,
        incomes,
        expenditures,
        sliders: sliders || expenditures,
        dataFetching: false,
        dataFetched: true
      };
    case FEEDBACK_SUBMITTED:
      return { ...state, feedbackSubmitting: true };
    case FEEDBACK_SUCCESS:
      return {
        ...state,
        feedbackRecieved: true,
        feedbackSubmitting: false
      };
    case AMOUNT_FIELD_CHANGED:
      dataType = action.payload.dataType;
      row = action.payload.row;
      input = action.payload.input;

      // Map over different array in state,
      // based on dataType
      switch (dataType) {
        case 'incomes':
          return { 
            ...state,
            
            // Only change values to row in state that equals the specified row
            incomes: state.incomes.map((income, i) => ( i === row  ? { ...income, amount: input } : income ))
          };
        case 'expenditures':
          return {
            ...state,
            expenditures: state.expenditures.map((expenditure, i) => ( i === row  ? { ...expenditure, amount: input } : expenditure ))
          };
        default:
          return { ...state };
      }
    case FROM_AGE_FIELD_CHANGED:      
      dataType = action.payload.dataType;
      row = action.payload.row;
      input = action.payload.input;
      
      switch (dataType) {
        case 'incomes':
          return { 
            ...state,
            incomes: state.incomes.map((income, i) => ( i === row  ? { ...income, from_age: input } : income ))
          };
        case 'expenditures':
          return {
            ...state,
            expenditures: state.expenditures.map((expenditure, i) => ( i === row  ? { ...expenditure, from_age: input } : expenditure ))
          };
        default:
          return { ...state };
      }
      case TO_AGE_FIELD_CHANGED:      
        dataType = action.payload.dataType;
        row = action.payload.row;
        input = action.payload.input;
        
        switch (dataType) {
          case 'incomes':
            return { 
              ...state,
              incomes: state.incomes.map((income, i) => ( i === row  ? { ...income, to_age: input } : income ))
            };
          case 'expenditures':
            return {
              ...state,
              expenditures: state.expenditures.map((expenditure, i) => ( i === row  ? { ...expenditure, to_age: input } : expenditure ))
            };
          default:
            return { ...state };
        }
    case SLIDER_CHANGED:
      const { sliderName, value } = action.payload
      return { 
        ...state,
        sliders: state.sliders.map(slider => ( slider.name === sliderName  ? { ...slider, amount: value } : slider ))
      };
    default:
      return state;
  }
};
