import React from 'react';
import { Link } from 'react-router-dom';
import AppNavigation from './AppNavigation';
import Button from '../components/Button';
import HatchLogo from '../assets/images/logo-hatch.png';

const AppHeader = () => {
  return (
    <header className="app-header">
      <Link className="app-header__logo-wrapper" to="/">
        <img className="app-header__logo" src={HatchLogo} alt="Hatch" />
      </Link>
      <AppNavigation 
        menu={[
          { name: 'Finance Plan', url: '/' },
          { name: 'Tips & Blog', url: '/blog' }
        ]}
      />
      <Button 
        variant="logout"
        url="/"
        text="Log Out"
      />
    </header>
  );
};

export default AppHeader;
