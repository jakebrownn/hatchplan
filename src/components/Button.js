import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  variant: PropTypes.string,
  externalSrc: PropTypes.bool,
};

const Button = ({ variant, externalSrc, text, url }) => {
  const vairantClass = ( variant ? `button button--${variant}` : `button` );

  if (externalSrc) {
    return <a className={vairantClass} href={`http://${url}`} target="_blank">{text}</a>;
  }

  return <Link className={vairantClass} to={url}>{text}</Link>;
};

Button.propTypes = propTypes;

export default Button;
