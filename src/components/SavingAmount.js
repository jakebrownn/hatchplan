import React, { Component } from 'react';
import { connect } from 'react-redux';

class SavingAmount extends Component {
  componentDidUpdate() {
    this.savingsTotal();
  }

  getTotalValues(dataArr) {
    let valuesArr = [];

    // Loop over each value of array
    for (let item of dataArr) {

      // Push each amount into array
      valuesArr.push(item.amount);
    }

    // Add together each number in array to get total expenditure
    return valuesArr.reduce((a, b) => a + b, 0);
  }

  // Calculate total savings
  savingsTotal() {
    const { sliders, expenditures } = this.props.user;
    const expenTotal = this.getTotalValues(expenditures);
    const slidersTotal = this.getTotalValues(sliders);
    const moneySaved = expenTotal - slidersTotal;
    return (0 < moneySaved ? moneySaved : 0);
  }

  render() {
    return (
      <p className="panel__text panel__text--bold">
        This means you're saving <span>{`£${this.savingsTotal()}`}</span> per month
      </p>
    );
  }
};

const mapStateToProps = ({ user }) => {
  return { user };
};

export default connect(mapStateToProps)(SavingAmount);
