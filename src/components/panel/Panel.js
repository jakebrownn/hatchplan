import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BeatLoader } from 'react-spinners';
import { connect } from 'react-redux';

const propTypes = {
  title: PropTypes.string.isRequired
};

class Panel extends Component {
  renderBody() {
    const { dataFetched } = this.props.user;

    if (dataFetched) {
      return this.props.children;
    }

    return (
      <div className="panel__loader-wrapper">
        <BeatLoader className="panel__loader" sizeUnit="px" size={8} color="#00576B" />
      </div>
    );
  }

  render() {
    const { title } = this.props;
    return (
      <div className="panel">
        <div className="panel__title-wrapper">
          <span className="panel__title">{title}</span>
        </div>
        <div className="panel__body">
          {this.renderBody()}
        </div>
      </div>
    );
  }
}

Panel.propTypes = propTypes;

const mapStateToProps = ({ user }) => {
  return { user };
};

export default connect(mapStateToProps)(Panel);
