import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  title: PropTypes.string.isRequired,
  dataType: PropTypes.string.isRequired
};

class PanelGroup extends Component {
  render() {
    const { title, dataType, children } = this.props;
    return (
      <div className="panel__group" data-type={dataType}>
        <span className="panel__group-heading">{title}</span>
        {children}
      </div>
    );
  }
}

PanelGroup.propTypes = propTypes;

export default PanelGroup;
