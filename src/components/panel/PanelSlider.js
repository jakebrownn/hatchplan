import React, { Component } from 'react';
import { connect } from 'react-redux';
import Slider from 'react-rangeslider';
import { sliderChanged } from '../../actions';
import PropTypes from 'prop-types';

const propTypes = {
  name: PropTypes.string.isRequired,
  amount: PropTypes.number.isRequired
};

class PanelSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      maxValue: 0
    };
  }

  componentDidMount() {
    const { name } = this.props;
    const { expenditures } = this.props.user;
    
    // Loop over expenditures
    for (let expense of expenditures) {
      
      // Set maxValue to the amount in expenditures
      if (expense.name === name) {
        this.setState({ maxValue: expense.amount });
      }  
    }
  }

  handleChange = (value) => {
    const { name } = this.props;

    this.props.sliderChanged(name, value);
  };

  render() {
    const { maxValue } = this.state;
    const { name, amount } = this.props;
    return (
      <div className="panel-slider">
        <span className="panel-slider__title">{name}</span>
        <div className="panel-slider__wrapper">
          <Slider
            value={amount}
            max={maxValue}
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  }
}

PanelSlider.propTypes = propTypes;

const mapStateToProps = ({ user }) => {
  return { user };
};

export default connect(mapStateToProps, { sliderChanged })(PanelSlider);
