import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../../actions';
import PropTypes from 'prop-types';

const propTypes = {
  row: PropTypes.number.isRequired,
  labelOne: PropTypes.string.isRequired,
  labelTwo: PropTypes.string.isRequired,
  labelThree: PropTypes.string.isRequired
};

class PanelRow extends Component {
  onInputChange(e) {
    const { row } = this.props;
    const input = e.currentTarget;
    const dataKey = input.parentNode.getAttribute('data-for');
    const dataType = input.closest('.panel__group').getAttribute('data-type');
    const inputValue = _.parseInt(input.value || 0);
    
    switch (dataKey) {
      case 'to_age':
        this.props.toAgeChanged(dataType, row, inputValue);
        break;
      case 'amount':
        this.props.amountChanged(dataType, row, inputValue);
        break;
      case 'from_age':
        this.props.fromAgeChanged(dataType, row, inputValue);
        break;
      default: 
        return;
    }
  }

  render() {
    const {
      labelOne,
      valueOne,
      labelTwo,
      valueTwo,
      labelThree,
      valueThree
    } = this.props;
    const labelOneText = _.startCase(labelOne);
    const labelTwoText = _.startCase(labelTwo);
    const labelThreeText = _.startCase(labelThree);
    return (
      <div className="panel-row">
        <div className="panel-row__field panel-row__field--wide">
          <label className="panel-row__label">{`${labelOneText}:`}</label>
          <div className="panel-row__input-wrapper" data-for={'amount'}>
            <span className="panel-row__money">£</span>
            <input
              className="panel-row__input panel-row__input--money"
              type="text"
              value={valueOne}
              onChange={(e) => this.onInputChange(e)}
            />
          </div>
        </div>
        <div className="panel-row__field">
          <label className="panel-row__label">{`${labelTwoText}:`}</label>
          <div className="panel-row__input-wrapper" data-for={labelTwo}>
            <input
              className="panel-row__input"
              type="number"
              value={valueTwo} 
              onChange={(e) => this.onInputChange(e)}
            />
          </div>
        </div>
        <div className="panel-row__field">
          <label className="panel-row__label">{`${labelThreeText}:`}</label>
          <div className="panel-row__input-wrapper" data-for={labelThree}>          
            <input
              className="panel-row__input"
              type="number"
              value={valueThree}
              onChange={(e) => this.onInputChange(e)}
            />
          </div>
        </div>
      </div>
    );
  }
}

PanelRow.propTypes = propTypes;

const mapStateToProps = ({ user }) => {
  return { user };
};

export default connect(mapStateToProps, actions)(PanelRow);
