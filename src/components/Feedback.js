import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { BeatLoader } from 'react-spinners';
import { submitFeedback } from '../actions';
import ThumbUpEmpty from '../assets/images/thumb-up-empty.svg';
import ThumbUpFill from '../assets/images/thumb-up-fill.svg';
import ThumbDownEmpty from '../assets/images/thumb-down-empty.svg';
import ThumbDownFill from '../assets/images/thumb-down-fill.svg';

class Feedback extends Component {
  constructor(props) {
    super(props);
    this.state = {
      thumbUpHover: false,
      thumbDownHover: false
    };
  }

  handleThumbHover(e) {
    const type = e.currentTarget.getAttribute('data-type');
    const { thumbUpHover, thumbDownHover } = this.state;

    // setState for thumb that is hovered
    switch (type) {
      case 'positive':
        this.setState({ thumbUpHover: !thumbUpHover });
        break;
      case 'negative':
        this.setState({ thumbDownHover: !thumbDownHover });
        break;
      default:
        return;
    }
  }

  thumbUpSrc() {
    const { thumbUpHover } = this.state;
    return ( thumbUpHover ? ThumbUpFill : ThumbUpEmpty );
  }

  thumbDownSrc() {
    const { thumbDownHover } = this.state;
    return ( thumbDownHover ? ThumbDownFill : ThumbDownEmpty );
  }

  handleThumbClick(e) {
    const type = e.currentTarget.getAttribute('data-type');

    this.props.submitFeedback(type);
  }

  renderContent() {
    const { feedbackRecieved, feedbackSubmitting } = this.props.user;

    // Render content for each phase of submitting feedback
    switch (true) {
      case feedbackSubmitting:
        return <BeatLoader className="feedback__loader" sizeUnit="px" size={8} color="#00576B" />;
      case feedbackRecieved: 
        return <span className="feedback__text">Your feedback has been recieved.</span>;
      default:
        return (
          <Fragment>
            <span className="feedback__text feedback__text--blue">Was this helpful?</span>
            <img 
              className="feedback__thumb"
              data-type="positive"
              src={this.thumbUpSrc()}
              alt="Thumbs Up"
              onClick={(e) => this.handleThumbClick(e)}
              onMouseEnter={(e) => this.handleThumbHover(e)}
              onMouseLeave={(e) => this.handleThumbHover(e)}
            />
            <img
              className="feedback__thumb"
              data-type="negative"
              src={this.thumbDownSrc()}
              alt="Thumbs Down"
              onClick={(e) => this.handleThumbClick(e)}
              onMouseEnter={(e) => this.handleThumbHover(e)}
              onMouseLeave={(e) => this.handleThumbHover(e)}
            />
          </Fragment>
        );
    }
  }

  render() {
    return (
      <div className="feedback">
        <div className="feedback__wrapper">
          {this.renderContent()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ user }) => {
  return { user };
};

export default connect(mapStateToProps, { submitFeedback })(Feedback);
