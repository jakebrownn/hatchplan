import React from 'react';
import AppHeader from './AppHeader';
import PropTypes from 'prop-types';

const propTypes = {
  variant: PropTypes.string.isRequired
};

const AppContent = ({ variant, children }) => {
  return (
    <div className="app-body">
      <AppHeader />
      <main className={`screen screen--${variant}`}>
        {children}
      </main>
    </div>
  );
};

AppContent.propTypes = propTypes;

export default AppContent;
