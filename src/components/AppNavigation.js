import React from 'react';
import { PropTypes } from 'prop-types';
import { NavLink } from 'react-router-dom';

const propTypes = {
  menu: PropTypes.array.isRequired
};

const AppNavigation = ({ menu }) => {
  return (
    <nav className="app-nav">
      <ul className="app-nav__list">
        { menu.map(item => {
          return (
            <li className="app-nav__item" key={item.url}>
              <NavLink
                className="app-nav__url"
                exact
                to={item.url}
                activeClassName="active"
              >
                {item.name}
              </NavLink>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

AppNavigation.propTypes = propTypes;

export default AppNavigation;
