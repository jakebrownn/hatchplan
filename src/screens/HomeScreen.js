import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import AppContent from '../components/AppContent';
import { Panel, PanelGroup, PanelRow, PanelSlider } from '../components/panel';
import Button from '../components/Button';
import Feedback from '../components/Feedback';
import SavingAmount from '../components/SavingAmount';

class HomeScreen extends Component {
  componentDidMount() {
    this.props.fetchData();
  }

  componentDidUpdate() {
    const { incomes, expenditures, sliders } = this.props.user;

    // Do not set data localStorage unless it has been set in state
    if ((incomes.length) && (expenditures.length)) {
      const hatchData = { incomes, expenditures, sliders };

      localStorage.setItem("hatchData", JSON.stringify(hatchData));
    }
  }

  render() {
    const {
      incomes,
      expenditures,
      sliders
    } = this.props.user;
    return (
      <AppContent variant="finance-plan">
        <Panel title="Your Income & Spend">
          <PanelGroup title="Annual income" dataType="incomes">
            { incomes.map((income, i) => {
              const { name, amount, from_age, to_age } = income;
              const keysArr = Object.keys(income);
              return (
                <PanelRow
                  key={`input-${name}`}
                  row={i}
                  labelOne={name}
                  valueOne={amount}
                  labelTwo={keysArr[1]}
                  valueTwo={from_age}
                  labelThree={keysArr[2]}
                  valueThree={to_age}
                />
              );
            })}
          </PanelGroup>
          <PanelGroup title="Monthly spending" dataType="expenditures">
            { expenditures.map((expense, i) => {
              const { amount, from_age, to_age, name } = expense;
              const keysArr = Object.keys(expense);
              return (
                <PanelRow 
                  key={`input-${name}`}
                  row={i}
                  labelOne={name}
                  valueOne={amount}
                  labelTwo={keysArr[1]}
                  valueTwo={from_age}
                  labelThree={keysArr[2]}
                  valueThree={to_age}
                />
              );
            })}
          </PanelGroup>
        </Panel>
        <Panel title="Spend Less">
          <p className="panel__text">Try reducing your monthly spending to see how your forecase could improve!</p>
          { sliders.map(slider => {
            const { name, amount } = slider;
            return (
              <PanelSlider 
                key={`slider-${name}`}
                name={name}
                amount={amount}
              />
            );
          })}
          <SavingAmount />
          <Button
            externalSrc
            text="Find ways to save"
            url="www.google.co.uk"
          />
          <Feedback />
        </Panel>
      </AppContent>
    );
  }
}

const mapStateToProps = ({ user }) => {
  return { user };
};

export default connect(mapStateToProps, actions)(HomeScreen);
