import React, { Component } from 'react';
import AppContent from '../components/AppContent';

class BlogScreen extends Component {
  render() {
    return (
      <AppContent variant="blog">
        <h1>Blog Page</h1>
      </AppContent>
    )
  }
};

export default BlogScreen;
